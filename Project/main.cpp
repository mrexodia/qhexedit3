#include <QApplication>
#include "MainWindow.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationName("QHexedit");
    app.setOrganizationName("QHexEdit");

    MainWindow *mainWin = new MainWindow;
    mainWin->show();

    return app.exec();
}
